FROM python:3.6.5-alpine3.7
LABEL maintainer="Denis Dubovitskiy <denisdubovitskiy@yandex.ru>"
# Dependency hell for lxml, psycopg2
RUN apk update && \
    apk --no-cache add \
    gcc libxml2 python3-dev \
    libxslt-dev libc-dev jpeg-dev \
    postgresql-dev && \
    pip3 install pipenv && \
    mkdir /app
COPY Pip* /app/
WORKDIR /app
RUN pipenv install && rm -rf /app/*
