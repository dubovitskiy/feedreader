Docker role
=========

Role which installs and configures Docker

Role Variables
--------------

`docker_daemon_config`: {}

Example Playbook
----------------

    - hosts: servers
      roles:
         - docker

License
-------

MIT

Author Information
------------------

Denis Dubovitskiy
