image:
	docker rmi --force feedreader:1.0.1
	docker build --tag feedreader:1.0.1 --file feedreader.dockerfile .

clean_image:
	docker rmi --force feedreader

run:
	docker-compose up

cleanup:
	docker-compose rm --force

migrations:
	docker-compose exec web pipenv run /app/manage.py migrate

workers:
	DJANGO_SETTINGS_MODULE=feedreader.settings_dev pipenv run  celery -A feedreader.celery:app worker -l INFO

rundev:
	docker-compose -f docker-compose-dev.yml up -d

stopdev:
	docker-compose -f docker-compose-dev.yml stop

rmdev:
	docker-compose -f docker-compose-dev.yml rm -f
	sudo rm -rf ./docker-data

purge:
	DJANGO_SETTINGS_MODULE=feedreader.settings_dev pipenv run  celery -A feedreader.celery:app purge -f

save:
	docker build --no-cache --tag feedreader:1.0.2 --file feedreader.prod.dockerfile .
	docker save feedreader:1.0.2 > ansible/roles/feedreader-image/files/feedreader-1.0.2.tar
