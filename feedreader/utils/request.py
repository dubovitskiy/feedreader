import random
import requests

from django.conf import settings


class Requests(requests.Session):
    """Requests session with randomised user agent."""

    def __init__(self):
        super().__init__()
        self.headers['User-Agent'] = random.choice(settings.USER_AGENTS)
