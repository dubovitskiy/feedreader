from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from feedreader.sources.models import RssSource, ArticleUrl, Article


@admin.register(RssSource)
class SourceAdmin(admin.ModelAdmin):
    list_display = ('title_or_default', 'last_update', 'state')
    search_fields = ('link', 'title')
    readonly_fields = ('last_update',)


@admin.register(ArticleUrl)
class ArticleUrlAdmin(admin.ModelAdmin):
    list_display = ('url', 'state', 'article_link')
    readonly_fields = ('url', 'state', 'article_link')


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    actions = (
        'show',
        'hide',
        'remove',
        'reset_views',
        'reset_stars'
    )
    list_display = (
        'title',
        'source',
        'site_link',
        'views',
        'stars',
        'is_visible'
    )
    readonly_fields = (
        'title_html',
        'authors',
        'publish_date',
        'top_image',
        'origin',
        'source',
        'views',
        'stars',
        # Need to be defined
        'top_image_html',
        'content_html',
        'top_image_href',
        'article_href',
        'summary_html',
        'site_link'
    )
    fieldsets = (
        (None, {
            'fields': (
                'title_html',
                'authors',
                'summary_html',
                'content_html',
                'source',
                'publish_date',
                'top_image_href',
                'article_href',
                'views',
                'stars'
            )
        }),
        (_('Raw'), {
            'classes': ('collapse',),
            'fields': ('content', 'summary'),
        }),
    )

    def show(self, _, queryset):
        queryset.update(is_visible=True)
    show.short_description = _('Make visible')

    def hide(self, _, queryset):
        queryset.update(is_visible=False)
    hide.short_description = _('Make hiden')

    def remove(self, _, queryset):
        queryset.delete()
    remove.short_description = _('Remove')

    def reset_views(self, _, queryset):
        queryset.update(views=0)
    reset_views.short_description = _('Reset views counter')

    def reset_stars(self, _, queryset):
        queryset.update(stars=0)
    reset_stars.short_description = _('Reset stars counter')
