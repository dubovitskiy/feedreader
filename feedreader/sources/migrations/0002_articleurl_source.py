# Generated by Django 2.0.5 on 2018-05-24 12:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sources', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='articleurl',
            name='source',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sources.RssSource', verbose_name='Source'),
            preserve_default=False,
        ),
    ]
