from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class SourcesConfig(AppConfig):
    name = 'feedreader.sources'
    verbose_name = _('RSS sources')
