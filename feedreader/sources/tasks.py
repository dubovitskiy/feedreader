from datetime import timedelta

import celery
import feedparser
import requests
from celery import group, Task
from celery.exceptions import Reject
from celery.utils.log import get_task_logger
from django.conf import settings
from django.utils import timezone
from newspaper import ArticleException

from feedreader.sources.cleaner import clean_html
from feedreader.sources.models import Article
from feedreader.sources.models import ArticleUrl
from feedreader.sources.models import RssSource
from feedreader.sources.utils import Feed
from feedreader.sources.utils import download_article
from feedreader.sources.utils import parse_article_html
from feedreader.utils.request import Requests

logger = get_task_logger(__name__)


@celery.shared_task
def find_new_articles():
    """Checks updates for each RSS source."""
    today = timezone.now().replace(tzinfo=None)
    today = today - timedelta(days=1)
    # article_urls_list = []
    iterator = RssSource.objects.filter(state=RssSource.STATE_REACHABLE).iterator(chunk_size=10)
    for rss_source in iterator:
        try:
            feed = Feed(rss_source.link)
        except requests.exceptions.RequestException:
            rss_source.state = RssSource.STATE_UNREACHABLE
            rss_source.last_update = timezone.now()
            rss_source.save()
            logger.error(f'Could not get an rss feed {rss_source.link}')
            continue
        rss_source.title = feed.title
        rss_source.last_update = timezone.now()
        rss_source.save()
        fresh_articles = feed.fresh_entries_from(today)
        article_urls = [ArticleUrl(url=a.link, source=rss_source) for a in fresh_articles]
        ArticleUrl.objects.bulk_create(article_urls)  # temporary
        # article_urls_list.extend(article_urls)
    # ArticleUrl.objects.bulk_create(article_urls_list)


@celery.shared_task
def download_new_articles():
    article_urls_q = ArticleUrl.objects.filter(state=ArticleUrl.STATE_INITIAL)
    article_url_ids = tuple(article_urls_q.values_list('pk', flat=True)[:10])
    ArticleUrl.objects.filter(pk__in=article_url_ids).update(state=ArticleUrl.STATE_PROCESSING)
    tasks = [parse_article.subtask((url_id,)) for url_id in article_url_ids]
    tasks_group = group(tasks)
    tasks_group.apply_async()
    return True


@celery.shared_task
def check_updates():
    sources = RssSource.objects.all()
    for source in sources:
        response = feedparser.parse(source.link)
        rss_feed = response.feed
        source.title = rss_feed.get('title', source.title)
        source.last_update = timezone.now()
        source.save()

        articles = response.get('entries')

        if not articles:
            continue

        for article in articles:
            if 'link' not in article:
                continue
            parse_article.delay(source.pk, article['link'])
    return True


class LogErrorsTask(Task):
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        logger.error(args, kwargs)
        super().on_failure(exc, task_id, args, kwargs, einfo)


@celery.shared_task(time_limit=60, base=LogErrorsTask)
def parse_article(article_url_pk):
    """Get article content by the given link and store it in the database."""
    article_url = ArticleUrl.objects.select_related('source').get(pk=article_url_pk)
    source = article_url.source
    logger.debug(f'Starting to parse article for "{source.title}" ({article_url.url})')
    try:
        logger.debug(article_url.url)
        content = download_article(article_url.url)
    except Exception as exc:
        logger.exception(f'Exception raised when tried to download an article "{article_url.url}"')
        article_url.fail()
        article_url.save()
        raise Reject(exc)
    logger.debug(f'Article {article_url.url} downloaded')
    try:
        remote_article = parse_article_html(content)
    except ArticleException as exc:
        logger.exception(f'Unable to parse an article "{article_url.url}"')
        article_url.fail()
        article_url.save()
        raise Reject(exc)
    logger.debug(f'Article {article_url.url} parsed')

    article_safe_html = clean_html(remote_article.article_html)

    if not len(remote_article.title):
        logger.debug(f'Article {article_url.url} doesn\'t have a title')
        article_url.fail()
        article_url.save()
        try_resolve_redirect.delay(article_url.pk)
        return Reject(ValueError('No title'))

    article = Article(
        title=remote_article.title,
        authors=', '.join(remote_article.authors),
        content=article_safe_html,
        publish_date=remote_article.publish_date,
        summary=remote_article.summary,
        origin=article_url.url,
        top_image=remote_article.top_image,
        source=source
    )
    logger.debug(f'Saving article {article.title}')
    article.save()
    article_url.delete()
    logger.debug(f'Article {article.title} has been saved')
    return True


@celery.shared_task
def remove_old_articles():
    deadline = timezone.now() - timedelta(days=settings.TRASH_HOLDING_PERIOD)
    Article.objects.filter(stars=0, publish_date__lt=deadline).delete()


@celery.shared_task
def try_resolve_redirect(article_url_pk):
    article_url = ArticleUrl.objects.get(article_url_pk)
    response = Requests().get(article_url.url)
    article_url.url = response.request.url
    article_url.state = ArticleUrl.STATE_INITIAL
    article_url.save()
    return True
