from django.urls import path

from feedreader.sources import views

urlpatterns = [
    path(r'<int:article_pk>', views.article_detail)
]
