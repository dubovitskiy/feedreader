import re
from bleach.sanitizer import Cleaner
from html5lib.filters.base import Filter


class ArticleFilter(Filter):
    def __iter__(self):
        for token in Filter.__iter__(self):
            if token['type'] in ['StartTag', 'EmptyTag'] and token['data']:
                if token['name'] == 'a':
                    token['data'][(token['namespace'], 'target')] = 'blank_'
            if token['type'] == 'SpaceCharacters' and token['data']:
                token['data'] = re.sub(r"\W", "", token['data'])
            yield token


ALLOWED_TAGS = [
    'a',
    'abbr',
    'acronym',
    'b',
    'blockquote',
    'code',
    'em',
    'i',
    'li',
    'ol',
    'strong',
    'ul',
    'img',
    'p',
    'br'
]

ALLOWED_ATTRIBUTES = {
    'a': ['href', 'title', 'target'],
    'abbr': ['title'],
    'acronym': ['title'],
    'img': ['src']
}

cleaner = Cleaner(
    tags=ALLOWED_TAGS,
    attributes=ALLOWED_ATTRIBUTES,
    filters=[ArticleFilter],
    strip=True,
)


def clean_html(html):
    return cleaner.clean(html)


__all__ = ['clean_html']
