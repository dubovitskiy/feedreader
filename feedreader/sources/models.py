from django.db import models
from django.utils import timezone
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _


class RssSource(models.Model):
    """RssSource is responsible for storing links to RSS feeds."""

    STATE_REACHABLE = 0
    STATE_UNREACHABLE = 1

    STATE_CHOICES = (
        (STATE_REACHABLE, _('Reachable')),
        (STATE_UNREACHABLE, _('Unreachable')),
    )

    title = models.CharField(
        verbose_name=_('Title'),
        max_length=500,
        blank=True,
        null=True
    )

    description = models.TextField(
        verbose_name=_('Description'),
        max_length=2000,
        blank=True,
        null=True
    )

    link = models.URLField(
        verbose_name=_('URL to RSS feed'),
        blank=False,
        null=False
    )

    last_update = models.DateTimeField(
        verbose_name=_('Last update'),
        default=timezone.now,
        null=False,
        blank=False
    )

    state = models.IntegerField(
        verbose_name=_('State'),
        default=STATE_REACHABLE,
        null=False,
        blank=False,
        choices=STATE_CHOICES,
    )

    class Meta:
        db_table = 'rss_sources'
        verbose_name = _('RSS feed')
        verbose_name_plural = _('RSS feeds')

    def __str__(self):
        if self.title:
            return f'{self.title}'
        return '%s' % _('No title yet')

    def __repr__(self):
        return f'<RssSource(link={self.link})>'

    def title_or_default(self):
        if self.title:
            return f'{self.title}'
        return _('No title')
    title_or_default.short_description = _('Title')


class ArticleUrl(models.Model):
    """Model for processing urls."""

    STATE_INITIAL = 0
    STATE_PROCESSING = 1
    STATE_FAILED = 2

    STATE_CHOICES = (
        (STATE_INITIAL, _('Initial')),
        (STATE_PROCESSING, _('Processing')),
        (STATE_FAILED, _('Failed')),
    )

    url = models.URLField(
        verbose_name=_('URL'),
        blank=False,
        null=False
    )

    state = models.IntegerField(
        verbose_name=_('State'),
        default=STATE_INITIAL,
        choices=STATE_CHOICES,
        blank=False,
        null=False
    )

    source = models.ForeignKey(
        verbose_name=_('Source'),
        to=RssSource,
        blank=False,
        null=False,
        on_delete=models.CASCADE
    )

    class Meta:
        db_table = 'article_urls'
        verbose_name = _('Article URL')
        verbose_name_plural = _('Article URLs')

    def __repr__(self):
        return f'<ArticleUrl(pk={self.pk} url={self.url})>'

    def __str__(self):
        return f'{self.url}'

    def start_process(self):
        self.state = self.STATE_PROCESSING

    def fail(self):
        self.state = self.STATE_FAILED

    def article_link(self):
        message = _('View ')
        return format_html(
            f'<a class="viewsitelink" '
            f'   href="{self.url}" '
            f'   target="blank_">'
            f'   {message}'
            f'</a>'
        )
    article_link.short_description = _('Actions')


class Article(models.Model):
    title = models.CharField(
        verbose_name=_('Title'),
        max_length=2000,
        blank=True,
        null=True
    )

    authors = models.CharField(
        verbose_name=_('Authors'),
        max_length=2000,
        blank=True,
        null=True,
    )

    content = models.TextField(
        verbose_name=_('Content'),
        blank=False,
        null=False,
    )

    summary = models.TextField(
        verbose_name=_('Summary'),
        blank=True,
        null=True
    )

    origin = models.URLField(
        verbose_name=_('Original article'),
        null=False,
        blank=False
    )

    top_image = models.URLField(
        verbose_name=_('Top image'),
        blank=True,
        null=True,
    )

    publish_date = models.DateTimeField(
        verbose_name=_('Publish date'),
        blank=True,
        null=True
    )

    source = models.ForeignKey(
        verbose_name=_('Source'),
        to=RssSource,
        on_delete=models.CASCADE
    )

    stars = models.IntegerField(
        verbose_name=_('Stars'),
        default=0,
        blank=False,
        null=False
    )

    views = models.IntegerField(
        verbose_name=_('Views count'),
        default=0,
        blank=True,
        null=True
    )

    is_visible = models.BooleanField(
        verbose_name=_('Visible'),
        default=True
    )

    class Meta:
        db_table = 'articles'
        verbose_name = _('Article')
        verbose_name_plural = _('Articles')

    def __str__(self):
        return f'{self.title}'

    def __repr__(self):
        return f'<Article(title={self.title})>'

    def star(self):
        """Increment stars counter."""
        self.stars += 1

    def unstar(self):
        """Decrement stars counter."""
        if self.stars > 0:
            self.stars -= 1

    def get_absolute_url(self):
        """Absolute site url."""
        return f'/articles/{self.pk}'

    def increase_views(self):
        """Increase views counter."""
        self.views += 1

    def top_image_html(self):
        return format_html(f'<img src={self.top_image} />')
    top_image_html.short_description = _('Image')
    top_image_html.empty_value_display = _('No image')

    def top_image_href(self):
        return format_html(f'<a href="{self.top_image}" target="blank_">{self.top_image}</a>')
    top_image_href.short_description = _('Image link')
    top_image_href.empty_value_display = _('No image')

    def content_html(self):
        return format_html(f'<div style="max-width: 600px">{self.content}</div>')
    content_html.short_description = _('Article content')

    def article_href(self):
        return format_html(f'<a href="{self.origin}" target="blank_">{self.origin}</a>')
    article_href.short_description = _('Original article')

    def title_html(self):
        return format_html(f'<h1>{self.title}</h1>')
    title_html.short_description = _('Title')

    def summary_html(self):
        return format_html(f'<p>{self.summary}</p>')
    summary_html.short_description = _('Summary')
    summary_html.empty_value_display = _('No summary')

    def site_link(self):
        url = self.get_absolute_url()
        message = _('View on site')
        return format_html(f'<a class="viewsitelink" href="{url}" target="blank_">{message}</a>')
    site_link.short_description = _('Actions')
