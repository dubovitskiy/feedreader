import time
from datetime import datetime

import feedparser
import newspaper
from django.conf import settings

from feedreader.utils.request import Requests


def download_article(url):
    """Download content of the page with Splash service."""
    params = {'url': url, 'resource_timeout': settings.SPLASH_RESOURCE_TIMEOUT}
    response = Requests().get(settings.SPLASH_HTML_URL, params=params)
    return response.content


def parse_article_html(html):
    """Get the content from given HTML."""
    article = newspaper.Article(url='', keep_article_html=True)
    article.set_html(html)
    article.parse()
    return article


def to_datetime(timetuple):
    if not timetuple:
        return None
    return datetime.fromtimestamp(time.mktime(timetuple))


class Entry:
    def __init__(self, title=None, authors=None, link=None,
                 published=None, summary=None):
        if not authors:
            authors = []
        self.authors_list = authors
        self.authors = ', '.join(authors)
        self.title = title
        self.link = link
        self.published_at = to_datetime(published)
        self.summary = summary

    def __repr__(self):
        return f'<Entry(link={self.link})>'

    def __str__(self):
        return self.title


class Feed:
    def __init__(self, url):
        self.url = url
        self._raw_xml = self._get_url_content()
        self._rss_feed = self._parse_rss_feed()
        self.updated_at = to_datetime(self._rss_feed.feed.get('updated_parsed'))
        self.title = self._param('title')
        self.subtitle = self._param('subtitle')
        self.description = self._param('description')
        self.entries = self._construct_entries()

    def __repr__(self):
        return f'<Feed(url={self.url})>'

    def __str__(self):
        return self.title

    def fresh_entries_from(self, from_date: datetime):
        fresh = []
        for entry in iter(self.entries):
            if not entry.published_at:
                continue
            if entry.published_at < from_date:
                continue
            fresh.append(entry)
        return fresh

    def _construct_entries(self):
        rss_entries_list = self._rss_feed.get('entries', [])
        entries = []
        for rss_entry in rss_entries_list:
            e = Entry(
                title=rss_entry.get('title'),
                link=rss_entry.get('link'),
                published=rss_entry.get('published_parsed'),
                summary=rss_entry.get('summary')
            )
            entries.append(e)
        return entries

    def _param(self, name):
        return self._rss_feed.feed.get(name)

    def _get_url_content(self):
        response = Requests().get(self.url, timeout=settings.RSS_RESPONSE_TIMEOUT)
        return response.content

    def _parse_rss_feed(self):
        return feedparser.parse(self._raw_xml)
