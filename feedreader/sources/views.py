from django.shortcuts import render_to_response

from feedreader.sources.models import Article
from feedreader.user_settings.models import UserSettings


def article_detail(request, *args, **kwargs):
    """Basic article page."""
    article = Article.objects.get(pk=kwargs.get('article_pk'), is_visible=True)
    article.increase_views()
    article.save()
    user_settings = UserSettings.objects.select_related('color_scheme').get(user=request.user)
    color_scheme = user_settings.color_scheme
    return render_to_response('articles_detail.html', context=locals())
