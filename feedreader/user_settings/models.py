from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class ColorScheme(models.Model):
    name = models.CharField(
        verbose_name=_('Name'),
        max_length=200,
        blank=False,
        null=False
    )
    background_color = models.CharField(
        verbose_name=_('Background color'),
        max_length=6,
        blank=False,
        null=False
    )

    text_header_color = models.CharField(
        verbose_name=_('Header color'),
        max_length=6,
        blank=False,
        null=False
    )

    text_paragraph_color = models.CharField(
        verbose_name=_('Paragraph color'),
        max_length=6,
        blank=False,
        null=False
    )

    class Meta:
        verbose_name = _('Color scheme')
        verbose_name_plural = _('Color schemes')

    def __str__(self):
        return f'{self.name}'

    def __repr__(self):
        return f'<ColorScheme(name={self.name})>'


class UserSettings(models.Model):

    LANGUAGE_EN = 'en'
    LANGUAGE_CHOICES = (
        (LANGUAGE_EN, _('English')),
    )

    user = models.OneToOneField(
        verbose_name=_('User'),
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )

    language_code = models.CharField(
        verbose_name=_('Language'),
        max_length=10,
        choices=LANGUAGE_CHOICES,
        blank=False,
        null=False,
        default=LANGUAGE_EN
    )

    color_scheme = models.ForeignKey(
        verbose_name=_('Color scheme'),
        to=ColorScheme,
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = _('User settings')
        verbose_name_plural = _('User settings')

    def __str__(self):
        return f'{self.user.username}\'s settings'

    def __repr__(self):
        return f'<UserSettings(user_id={self.user_id}>'
