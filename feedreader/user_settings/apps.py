from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UserSettingsConfig(AppConfig):
    name = 'feedreader.user_settings'
    verbose_name = _('User settings')
    verbose_name_plural = _('User settings')
