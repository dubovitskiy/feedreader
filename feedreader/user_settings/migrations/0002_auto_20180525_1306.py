# Generated by Django 2.0.5 on 2018-05-25 13:06

from django.db import migrations


def create_color_schemes(apps, *args, **kwargs):
    ColorScheme = apps.get_model('user_settings', 'ColorScheme')
    schemes = [
        ColorScheme(
            name='Sepia',
            text_header_color='5a4636',
            text_paragraph_color='5a4636',
            background_color='5a4636',
        ),
        ColorScheme(
            name='Bright',
            text_header_color='222222',
            text_paragraph_color='222222',
            background_color='fdfdfd',
        ),
        ColorScheme(
            name='Dark',
            text_header_color='cccccc',
            text_paragraph_color='cccccc',
            background_color='292929',
        )
    ]
    ColorScheme.objects.bulk_create(schemes)


class Migration(migrations.Migration):
    dependencies = [
        ('user_settings', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_color_schemes)
    ]
