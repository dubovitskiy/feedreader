from django.contrib import admin
from feedreader.user_settings import models


@admin.register(models.UserSettings)
class UserSettingsAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'color_scheme'
    )


@admin.register(models.ColorScheme)
class ColorSchemeAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'background_color',
    )
