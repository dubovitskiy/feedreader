from django.urls import path

from feedreader.catalog import views

urlpatterns = [
    path(r'', views.catalog_index)
]
