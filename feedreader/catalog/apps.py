from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CatalogConfig(AppConfig):
    name = 'feedreader.catalog'
    verbose_name = _('Catalog')  # ambiguous
    verbose_name_plural = _('Catalogs')
