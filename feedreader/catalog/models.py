from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from feedreader.sources.models import RssSource


class Category(models.Model):
    """Model for storing user categories."""

    user = models.ForeignKey(
        verbose_name=_('User'),
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    title = models.CharField(
        verbose_name=_('Title'),
        max_length=255,
        blank=False,
        null=False
    )
    feeds = models.ManyToManyField(
        verbose_name=_('Feeds'),
        to=RssSource,
    )

    class Meta:
        db_table = 'categories'
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        unique_together = (('user', 'title'),)

    def __str__(self):
        return f'{self.title}'

    def __repr__(self):
        return f'<Category(title={self.title})>'
