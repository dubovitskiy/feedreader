from django.shortcuts import render_to_response
from feedreader.catalog.models import Category


def catalog_index(request):
    categories = Category.objects.filter(user=request.user) \
        .exclude(feeds__title='') \
        .prefetch_related('feeds')
    return render_to_response('sources.html', context=locals())
