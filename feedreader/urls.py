from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.utils.translation import ugettext_lazy as _

admin.site.site_header = _('Feed reader')
admin.site.site_title = _('Feed reader')
admin.site.index_title = _('Dashboard')

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^articles/', include('feedreader.sources.urls')),
    url(r'^catalog/', include('feedreader.catalog.urls')),
    url(r'^rest-auth/', include('rest_auth.urls'))
]
