from .settings_dev import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv('FDR_DB_NAME', 'feedreader'),
        'USER': os.getenv('FDR_DB_USERNAME', 'postgres'),
        'PASSWORD': os.getenv('FDR_DB_PASSWORD', 'password'),
        'HOST': os.getenv('FDR_DB_HOST', 'postgres'),
        'PORT': os.getenv('FDR_DB_PORT', '5432')
    }
}

# Splash browser settings
# Deployed in Docker containers and available through haproxy
SPLASH_HOST = os.getenv('FDR_SPLASH_HOST', 'splash')
SPLASH_PORT = os.getenv('FDR_SPLASH_PORT', 8050)
SPLASH_SCHEME = os.getenv('FDR_SPLASH_SCHEME', 'http')
SPLASH_RESOURCE_TIMEOUT = os.getenv('FDR_SPLASH_RESOURCE_TIMEOUT', 2)
SPLASH_HTML_URL = f'{SPLASH_SCHEME}://{SPLASH_HOST}:{SPLASH_PORT}/render.html'
CELERY_BROKER_URL = 'amqp://feedreader:feedreader@rabbitmq:5672//'
